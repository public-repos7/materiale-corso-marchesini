#define CATCH_CONFIG_MAIN  // inserisce il main(). Da usare solo in un file .cpp
                           // per ogni eseguibile
#include "catch.hpp"

int a = 1;

TEST_CASE("uguale a 1", "[1]") {
  REQUIRE(a == 1);
}

TEST_CASE("uguale a 2", "[2]") {
  REQUIRE(a == 2);
}