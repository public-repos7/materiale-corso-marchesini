#define CATCH_CONFIG_MAIN  // inserisce il main(). Da usare solo in un file .cpp
                           // per ogni eseguibile
#include "catch.hpp"

TEST_CASE("modifiche variabili", "[variabili]") {
  int a = 5;
  REQUIRE(a == 5);
  SECTION("incremento") {
    a++;
    REQUIRE(a == 6);
  }
  SECTION("decremento") {
    a--;
    REQUIRE(a == 4);
    SECTION("raddoppio") {
      a *= 2;
      REQUIRE(a == 8);
    }
    SECTION("dimezzamento") {
      a /= 2;
      REQUIRE(a == 2);
    }
  }
}