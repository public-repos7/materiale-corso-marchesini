#define CATCH_CONFIG_MAIN  // inserisce il main(). Da usare solo in un file .cpp per ogni eseguibile
#include "catch.hpp"

unsigned int fattoriale(unsigned int number) {
  return number <= 1 ? 1 : fattoriale(number - 1) * number;
}

TEST_CASE("calcola correttamente i fattoriali", "[fattoriale]") {
  REQUIRE(fattoriale(1) == 1);
  REQUIRE(fattoriale(2) == 2);
  REQUIRE(fattoriale(3) == 6);
  REQUIRE(fattoriale(10) == 3628800);
}